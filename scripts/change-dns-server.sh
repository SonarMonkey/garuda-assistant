#!/bin/bash
chattr -i /etc/resolv.conf
cd /etc
cp -rf resolv.conf resolv.conf.bak
echo "Applying DNS server changes…"
sed -i -e "/nameserver*/d" /etc/resolv.conf ;
sed -i '$a\' /etc/resolv.conf ;
echo "nameserver ${1}" >> /etc/resolv.conf ;
echo "Prevent resolv.conf from being overwritten"
chattr +i /etc/resolv.conf

echo "New DNS server enabled!"

